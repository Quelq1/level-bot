const package = require('./package.json');
console.log('Starting level-bot V' + package.version);
console.log('Now loading dependencies');

const meow = require('meow');
const fs = require('fs');
const yaml = require('yaml');
const discord = require('discord.js');

const cli = meow(`
    Usage
      $ index.js
 
    Options
      --config, -c  Load another config file than config.yaml (default)
`, {
    flags: {
        config: {
            type: 'string',
            alias: 'c',
            default : 'config.yaml'
        }
    }
});

console.log(cli.flags.config)
const {
    Token
} = yaml.parse(fs.readFileSync(cli.flags.config))